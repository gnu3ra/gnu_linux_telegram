Rules for GNU/Linux Chat
---

- Be nice to each other
- English only, if we can't mod it it isn't allowed
- No [flaming](https://www.urbandictionary.com/define.php?term=flame)
- No [shitposting](https://www.urbandictionary.com/define.php?term=Shit+Posting), [spam](https://upload.wikimedia.org/wikipedia/commons/0/09/Spam_can.png), etc
- Your bots are not allowed, don't add them
- Use constructive messages whenever possible

Violations to any rule may result in deleted content

If an Admin finds anyone doing any of these things they will give a
warning, a second offence will result in removal from the chat.

---
#### For Admins:

- All user rules apply to you
- No censoring unless rules are broken, please screenshot before
  deleting messages so we know what you deleted, and you can prove that
  the content you deleted should have been deleted

If you see an Admin breaking any of these rules, the user rules, or
if you have any issues or concerns, please message me and I will respond
and try to resolve your issue as quickly as possible.

---
- Telegram          [@codebam](https://telegram.me/codebam)
- Email and XMPP    [codebam@riseup.net](mailto:codebam@riseup.net)
